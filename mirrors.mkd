Title: Mirrors
CSS: /css/main.css

#{include head}

# Mirrors

## Contributing a mirror

Contributing a mirror to Exherbo is fairly simple. It consists of syncing
with the master mirror every so often via `rsync`, adding an entry to
the `arbor` line in ::arbor's [mirrors.conf], and submitting a patch to
Gitlab, which adds the mirror.

Mirrors with HTTP(S), FTP, with IPv4 and v6 (or both) are currently
accepted.

For example, a cronjob executing `rsync` at 02:42, every day:

    42 2 * * * flock /srv/www/distfiles.gov/mirror/exherbo-sync.lock rsync -ruvtE rsync://distfiles.exherbo.org/exherbo-mirror /srv/www/distfiles.gov/mirror/exherbo

`flock` is used to prevent long `rsync` operations from starting when the
file lock is currently used; otherwise it could result in multiple `rsync`
instances running at a time.

`rsync -ruvtE`: rsync recursively, update the local directory, verbosely,
transfer modification times, and preserve executability.

Obviously, you should not copy this example blindly.

In addition, please don't synchronize much more than once a day. The
main mirror only updates once a day. Your IP may be banned if you are
mean to the mirror.

Practice common courtesy and don't be rude to our servers. Thank you. :)

## List of distfile mirrors

Mirror URL                                    | Owner                                     | Location           | Notes                              |
--------------------------------------------- | ----------------------------------------- | ------------------ | ---------------------------------- |
<http://distfiles.exherbo.org/distfiles/>     | Kim Højgaard-Hansen <kimrhh@exherbo.org>  | Germany            | Master mirror, IPv4 only           |
<http://mirrors.dotsrc.org/exherbo/>          | Kim Højgaard-Hansen <kimrhh@exherbo.org>  | Denmark            | Dual IP stack                      |
<ftp://mirrors.dotsrc.org/exherbo/>           | Kim Højgaard-Hansen <kimrhh@exherbo.org>  | Denmark            | Dual IP stack                      |

## List of git repository mirrors

Mirror URL                                    | Owner                                     | Location           | Notes                              |
--------------------------------------------- | ----------------------------------------- | ------------------ | ---------------------------------- |
<https://git.exherbo.org/>                    | Exherbo Developers                        | Denmark            | Master mirror, IPv4 only           |
<https://git.nightly.network/exherbo/>        | Irvin Choi <simdol8080@gmail.com>         | United States      | IPv4, HTTPS only                   |


## Changing mirror on Paludis

By default, Paludis uses `git.exherbo.org` to sync repositories with git
protocol. To use different repository, simply edit `/etc/paludis/repositories/{repository_name}.conf`'s
`sync` value into repository you want to use.

For example, steps for changing git mirror from `https://git.exherbo.org/` to `https://git.nightly.network/exherbo/`
for `arbor` would be:

1) Open `/etc/paludis/repositories/arbor.conf` with text-editor of your choice.

2) Under location, replace `sync` value so that:

`sync = git+https://git.exherbo.org/arbor.git`

would be:

`sync = git+https://git.nightly.network/exherbo/arbor.git`.

3) Save the file, and exit.

4) Sync the repository, `cave sync arbor`.

That's it! If you have multiple repositories that you wish to change the mirror
of, consider using `sed` to make your life easier.

[mirrors.conf]: //git.exherbo.org/arbor.git/tree/metadata/mirrors.conf

--
Copyright 2015-2016 Kylie McClain

#{include CC_3.0_Attribution}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
