Title: Graveyard
CSS: /css/main.css

#{include head}

Graveyard
=========

## Overview

If a package is found to be out of date and unmaintained or a
repository (user repositories, usually) has become unmaintained and no
other user is inclined to take over maintenance and management of that
package or repository, the appropriate procedure is to "bury" that
entity. Buried packages and repos are recorded in the [graveyard
repository](http://git.exherbo.org/graveyard.git "graveyard - git repo").

## Dexter

The most straightforward way of managing burials is to use the dexter
script maintained [here](http://git.exherbo.org/dexter.git "dexter -
git repo").

For proper syntax of .graveyard files, please see the dexter.rb
script. For further information on usage of dexter, see README in the
[dexter repository](http://git.exherbo.org/dexter.git "dexter -
git repo").

## Keys

Several keys show up in .graveyard files. Available keys and their
significance are described here:

Key          | Significance
------------ | ---------------------------------------------------------------------------------------------------------------------------
homepage     | Required.    One or more URIs, space separated                                                                             |
comment      | Recommended. Why was this package/repo buried?                                                                             |
commit-id    | When possible. The (shortened) id of the commit that removed this package (There is no commit-id for a buried repository.) |
description  | Recommended. Description of the package that was removed                                                                   |
removed-by   | Required. Name `<email>`                                                                                                   |
removed-from | Required. Repository that previously held this package. For repositories, this is "unavailable-unofficial"                 |


## Notes on graveyard usage

- If a repository has been buried and the original repo is no longer
  available (removed from github, or in any other way vanished),
  format-patches  to that repository
  are still available, in a sense. Developers with access to
  [summer](http://git.exherbo.org/summer) can retrieve any
  format-patches to a repo up to the point that summer ceased indexing
  that repository. This allows an exhumed package to maintain its git
  history. Requests for format-patches from buried repositories may be
  made in #exherbo.

--
Copyright 2013 A Frederick Christensen

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
