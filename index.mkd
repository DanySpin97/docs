Title: Documentation
CSS: /css/main.css

#{include head}

# Documentation

## For Users

### Getting started
* [Expectations](expectations.html)
* [Features](features.html)
* [FAQ](faq.html)
* [Install Guide](install-guide.html)

### Paludis
* [cave, the main Paludis client](http://paludis.exherbo.org/clients/cave.html)
* [Configuration](http://paludis.exherbo.org/configuration/index.html)
* [Paludis-FAQ](http://paludis.exherbo.org/faq/index.html)

### Misc
* [KDE on Exherbo](kde.html)
* [Unavailable repository](//ciaranm.wordpress.com/2008/06/12/dealing-with-lots-of-repositories/)
* [Unwritten repository](//ciaranm.wordpress.com/2008/10/06/dealing-with-unwritten-packages/)
* [Systemd HowTo](systemd.html)
* [Without Systemd](without-systemd.html)

### Multiarch
* [Multiarch Migration Guide](multiarch/multiarch.html)
* [Multiarch: Adding another target architecture](multiarch/multiarch-new-target.html)

## For Contributors/Developers

### Contributing Documentation
* [Contributing to Exherbo](contributing.html)
* [Using Gitlab](gitlab.html)
* [Using the patchbot](patchbot.html)
* [Workflow](workflow.html)

### Exheres Writing
* [Alternatives](eapi/alternatives.html)
* [Exheres for smarties (How to write Exhereses)](eapi/exheres-for-smarties.html)
* [Cross-compiling support in exhereses](cross.html)
* [Multiarch for developers](multiarch.txt)
* [Multibuild for developers](multibuild.html)
* [Providers and Virtuals](eapi/providers-and-virtuals.html)

### Exlibs Documentation
* [flag-o-matic.exlib](exlibs/flag-o-matic.html)
* [scm.exlib](exlibs/scm.html)
* [toolchain-funcs.exlib](exlibs/toolchain-funcs.html)

### Misc
* [Bootstrapping Exherbo](bootstrapping.html)
* [Emacs](emacs.html)
* [Graveyard](graveyard.html)
* [Mirrors](mirrors.html)
* [Multiarch TODO list](multiarch-TODO.html)
* [Project ideas](project-ideas.html)

#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
