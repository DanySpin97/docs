commit ae7e6436864e6f0d86e8d568fc87ae329a5ce58d (HEAD, origin/master, origin/HEAD, master)
Author:     Bo Ørsted Andresen <zlin@exherbo.org>
AuthorDate: Mon Feb 10 22:30:14 2014 +0100
Commit:     Bo Ørsted Andresen <zlin@exherbo.org>
CommitDate: Mon Feb 10 22:30:19 2014 +0100

    Add ::japaric.

diff --git a/playboy/exherbo-unofficial/config/repositories/japaric.conf b/playboy/exherbo-unofficial/config/repositories/japaric.conf
new file mode 100644
index 0000000..df17021
--- /dev/null
+++ b/playboy/exherbo-unofficial/config/repositories/japaric.conf
@@ -0,0 +1,2 @@
+location = ${my_unofficial_repos}/japaric
+sync = git+https://github.com/japaric/exheres
