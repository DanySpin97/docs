Title: Multiarch TODO (aka Things we want for crossmas)
CSS: /css/main.css

#{include head}

The big multiarch TODO list
===========================

* This will become a table of contents (this text will be scraped).
{:toc}

## Labels
 
### build-time@target

Usecase: headers, `foo-config`

As opposed to `build-time@native` for build tools.

Proposed: rename `build:` to `build-tools:`, use `build:` for headers?

Drawback: `build:` still isn't descriptive. Maybe keep `build:` as is and come
up with a name for `build-because-headers-or-stuff:`


### run-time@native

Usecase:
  - data-only deps (e.g. `fonts/corefonts`)
  - calling binaries at runtime, e.g. [steam] ships binaries and a bash script.
    The script uses `bash` and calls `curl`, `zenity`, `desktop-file-utils`, ...
    all of which are needed only for the native arch. It also needs 32bit
    libraries, so ideally we would cross-install it to the i686 installed
    repository. However, it is ran in a multibuild-like scenario, so installing
    these dependencies for the target is unnecessary.

Problem: Breaks the "just switch /usr/host to switch arches" usecase.

Solution: add a `cave-resolve` switch like `--dependencies-to-slash` that lets
the user decide for which arch he wants these dependencies built.

native: multibuild-like usecase

target: cross-compiling for raspberry pi usecase

both: ?

[steam]: //git.exherbo.org/games.git/tree/packages/distribution/steam/steam-1.0.0.49-r2.exheres-0#n40

### build-cross

Usecase: cross compiling `python` needs a native `python` of the same `SLOT`,
`icu` needs native `icu`, etc

a `build-time`, `native-arch` type dependency that only gets pulled in for
non-native builds

Considerations:
  - Are there any known cases that don't follow the above pattern?
  - Can we just mandate that cross-compiling `foo` requires native `foo` to be present as well? (Either via policy or paludis behaviour)

## Paludis things

### Questions to answer before merging

    zlin | compnerd: can you list what the current labels do with regards to multiple targets?

### Ordererererererer

Paludis shouldn't consider native installed packages for ordering `build+run`
deps when cross-compiling packages.

Testcase: `systemd` <-> `util-linux[systemd]` is a circular dependency.

    cave resolve -mx systemd

doesn't complain about that.

       mixi | (compnerd, (ciaranm): why doesn't paludis tell philantrop he has circular there e.g. systemd -> util-linux -> systemd)
    ciaranm | because he already has one of them installed and usable, maybe? or because he doesn't have a cycle?
       zlin | the one on / should suffice
       zlin | the native
       mixi | but they link to each others libraries
       mixi | they should be build+run deps (and are afair)
       zlin | pretty sure that's what the resolver thinks
       zlin | which probably leads back to needing more labels
    ciaranm | i dunno where compnerd made the resolver look for ordering
    ciaranm | i don't think he touched the ordererererererer
    ciaranm | which means it'll behave like a chroot, and always use stuff on / for ordering, i think
       zlin | my reasoning too
    ciaranm | in which case we have another reason cross isn't ready to merge

### fix-linkage -- -mx or fix-linkage -- -4 i686-pc-linux-gnu behaves funny

cave fix-linkage -- -4 i686-pc-linux-gnu checks what's broken on cross and then
reinstalls the default version. This should just ignore -4 because there's no
-mx and thus check what's broken on default and reinstall that.

cave fix-linkage -- -mx checks what's broken on default and then builds it on
cross. this requires there being only one cross target to pick from in order to
reproduce the problem. therefore it should check what's broken on that target
and rebuild that.

### Per target options

We want a way to set options per-target without different paludis environments.

Possibly also default options in profiles.

Either syntax or multiple files?

Possibly not as pressing if we decide on an alternative to parts (see below).

### Per target DOWNLOADS

Like we had with `platform:` or similar.

Thoughts: Maybe related to reworking profiles? per-target sub-profiles could set
an option we could use. I seem to remember ciaranm saying that differing
profiles between packages won't work? Also we don't know the target currently
while fetching.

### per target world files

Or something that works similarly.

Currently there is no way to track what was explicitly installed for a
cross-compile target, like there is with the `world` set for the native target.

One must either cultivate such a set manually, use installed-slots to update the
cross compiled packages or just use the `world` set from the native target
installed repository, resulting in cross-compiling every package on the system.

Add some way to make this easier. Maybe per-target `world` files.

Note: the world set doesn't specify where things get installed, so one would
still have to use 

        cave resolve world::foo -mx -4foo

Potentially it could automatically use the `world` set from the targeted
installed repository (selected by `--cross-host/-4`)?

## Exheres things

### PLATFORMS
Does it still serve a purpose?

There seems to be pretty good consensus that PLATFORMS in its current state
serves no real purpose and may be better used by renaming to DISABLED_HOSTS
becoming a blacklist rather than whitelist of supported hosts.

  - the default should be "every triple is supported", unless the exheres says
    otherwise, to facilitate adding new targets.
  - a pure blacklist approach doesn't make sense. Is there really a package that
    builds on every triple ever invented, except the specific one that is
    blacklisted?  *No*
  - on the other hand, packages have a need to specify that they only work on
    one triple. e.g. binaries.

Proposal(moben):

  - A non-additive metadata variable `HOSTS`, which defaults to being empty.
  - As "no triple supported" makes no sense, we define empty `HOSTS` to mean
    _every triple is supported_.
  - If an exheres sets `HOSTS` to a non-empty space separated list then it only
    supports the listed triples.

This allows easily adding new targets as most exheres don't need changes, while
allowing those that only support a limited set of targets to easily specify
those. With a straight up blacklist approach, they would have to say
`DISABLED_HOSTS='* -supported-host'`

It can also be used when writing out pbins, to set their target triple.

Downside: 
  - Empty meaning _every triple is supported_ is surprising at first.
  - So is the fact that adding to the variable actually removes many targets.
    
    It should be thought of as overwriting the default instead of adding.

Open questions:

  - moben:
    - Do we allow wildcards?
      
      e.g. `i686-*-linux-gnu` might be useful for binaries which don't care
      about the vendor part.
    - Would any other parts of the triple benefit from wildcards?

  - zlin:
    - `I wonder if it should just match the first part of the triplet`

### A better way to deal with collisions

We have parts, but they aren't nice to use.

Have to copy mostly unchanged `expart` boilerplate to every exheres, breaking
the "cleanness" of the default exheres design, where a simple autotools using
package only needs metadata set to build correctly.

Have a default partition, depending on which parts are in `MYOPTIONS`? (or does
`expart` handle being called with a part that isn't in `MYOPTIONS`?)

Add `DEFAULT_SRC_INSTALL_PARTITIONS`.

For example, to place `bin/foo-config` in the `development` part instead of the
`binaries` part.

Maybe move the partitioning to a separate phase?

Some parts should be presumed. Technically `development` parts only need to be
`build@target:` presumed, `libraries` and `binaries` parts need to be
`build+run:` presumed.

##### Do we even want to use parts for everything?

The original idea was being able to uninstall parts (no pun intended) of a big
package without reinstall, not necessarily using them for everything.

Alternatives?

Some talk about reference counting in `exndbam`. Would necessitate keeping both
copies of colliding files somewhere.

Would be easier for exheres authors:

  - No partitioning
  - No presumed parts, no opting out of those via `[?part:foo]`

Easier for users as well, because there is less mucking about with `parts:`
options. It's unclear how much mucking about there would be with parts though,
maybe it isn't so bad.

Possibly less need for target-specific options (see above)

Avoids gradually slipping into "let's list every file we want to install
manually" binary distribution territory.

Pitfalls:
Need to keep the "last installed wins" semantic to make regular "file moves
between two packages" collisions work.
Shouldn't be a problem though, as colliding files between targets should be
identical. If they aren't then that needs to be fixed or the files moved to the
exec_prefix.

Additional advantage:
Easy checking if colliding files are identical.

### Get rid of \*\_cpu\_features?

compnerd wants this, he has convinced me as well. Others not so much.

User should be able to use `CFLAGS` to do the same, e.g. by adding `-msse` or
`-mno-sse`

Fix configure scripts to test building an `sse` sample with the user-supplied
`CLFAGS` and decide based on that to include their inline sse assembly or not,
instead of relying on an --enable-sse configure flag.

Upside: a single mechanism to control the use of cpu extensions for all
packages, regardless of them having a configure switch for it.

Downside:
  - potentially hard to sell to upstreams?
  - maybe harder for other buildsystems?

Idea(moben): In case upstreams prove particularly resistent to the change,
create an exlib that can do the compile check for each feature.
(NOTE: compnerd hates the idea)

`cpu-feature foo [bar] [baz]` like `option foo [bar] [baz]`, but checks if cpu
feature `foo` works with current `CC` and `*FLAGS` instead of if option `foo`
is enabled.

`cpu-feature_enable/with` likewise. Maybe also convenience functions for other
buildsystems, like `cmake_cpu-feature_enable`.

## Compilers
We need to canonicalise compiler names.
Packages currently hardcode gcc, g++, gas, gfortran into the build recipes.
These need to be eradicated in favour of the canonical compiler names (cc, c++,
as, fc respectively).

### Split alternatives for gcc
GCC provides a single alternative currently.  This should be split up on a
language boundary.  This means that we would have a cc, c++, fc (go?)
alternative which would allow for different versions and providers of these
compilers to be mixed and matched.

## slim LTO
ar/nm/ranlib needs to be settable to their gcc- prefixed equivalents for slim
LTO functionality. Alternatives also seem a good solution for this.

## Other
- work out how to enable tests for compatible hosts (e.g. i686-pc-linux-gnu on
  x86_64-pc-linux-gnu). Adjust handling of test deps accordingly, so that they
  are cross-compiled
- improve ASFLAGS/CFLAGS/CXXFLAGS/LDFLAGS handling
- re-enable glibc tests
- fix up zlib exheres to support clang again
- allow cross compilation of toolchain packages (yes, this is in addition to the
  targets:* suboption).
- why does libexec/paludis/utils/exheres-0/strip still do `exec strip`? it should
  be using `$(exhost --tool-prefix)strip`.

### fix multiload for steam

steam fails under multiload, unsure if anything else has the same problem.

As far as I know it launches a shell script that launches a shell script that
launches a 32bit binary or something along those line.

Questions:
  - Is there some sort of minimal testcase?
  - Is it unique to steam? Is their binary funny in some way?


        mixi | kimrhh: and I think multiload tries to solve the problem where binaries use the same loader path for different architectures
        mixi | kimrhh: but I never got it to work
        mixi | kimrhh: as the kernel rejects loaders if the machine type (as in ELF header e_machine) of the binary doesn't match the one of the loader

[IRC log from a debugging session](http://dev.exherbo.org/~moben/mixi_compnerd-multiload_debugging.txt)

### cave-import is broken with debug info splitting

    # mkdir -p crossimage/usr/x86_64-pc-linux-gnu/bin
    # echo 'int main(){}' | x86_64-pc-linux-gnu-cc -o crossimage/usr/x86_64-pc-linux-gnu/bin/exe -g -xc -
    # cave import --location crossimage cross/test -x
    *snip*
    n   cross/test:0::unpackaged 0 to ::installed-unpackaged
        build_options: symbols=compress dwarf_compress work=tidyup
        Reasons: target
    *snip*
    cave@1452120429: [WARNING merger.ndbam.warning] In thread ID '1185383':
    ... When merging 'cross/test-0:0::unpackaged' at 'crossimage' to InstalledUnpackagedRepository repository 'installed-unpackaged':
    ... When checking merge from 'crossimage' to '/':
    ... When checking merge from 'crossimage' to '/':
    ... When checking merge from 'crossimage/usr' to '/usr':
    ... When handling dir 'crossimage/usr/lib' to '/usr':
    ... Expected '/usr/lib' to be a directory but found a symlink to a directory
    *snip*
    >>> Merging to /
    =>> [dir] /usr
    =>> [dir] /usr/x86_64-pc-linux-gnu
    =>> [dir] /usr/x86_64-pc-linux-gnu/bin
    >-> [obj] /usr/x86_64-pc-linux-gnu/bin/exe
    =>> [dir] /usr/lib
    =>> [dir] /usr/x86_64-pc-linux-gnu/lib/debug
    =>> [dir] /usr/x86_64-pc-linux-gnu/lib/debug/usr
    =>> [dir] /usr/x86_64-pc-linux-gnu/lib/debug/usr/x86_64-pc-linux-gnu
    =>> [dir] /usr/x86_64-pc-linux-gnu/lib/debug/usr/x86_64-pc-linux-gnu/bin
    >-> [obj] /usr/x86_64-pc-linux-gnu/lib/debug/usr/x86_64-pc-linux-gnu/bin/exe.debug

It's pretty low priority though because miraculously it manages to merge
over the symlink without any issues and uninstalling just skips it because
`[!type]`. But /usr/lib ofcourse has no reason beeing in there.

--
Copyright 2015 Benedikt Morbach

#{include foot}

<!-- vim: set et ft=mkd spell spelllang=en sts=4 sw=4 ts=8 : -->
