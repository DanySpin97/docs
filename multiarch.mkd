Title: Multiarch
CSS: /css/main.css

#{include head}

# Multiarch

* This will become a table of contents (this text will be scraped).
{:toc}

"All problems in computer science can be solved by another layer of indirection"
    -- David Wheeler

# Synopsis
    multiarch attempts to further generalise the nascent implementation of
    cross-target installation introduced by multilib.  One of the biggest
    problems of the implementation of multiple targets introduced by multilib
    was the lack of a layer of indirection.  This has resulted in many places
    requiring special handling of paths and locations for libraries.  multiarch
    attempts to solve the problem by introducing another layer of indirection in
    the filesystem to account for the requirements of multiple ABIs on a single
    host.

# Benefits
    * co-installation of multiple (arbitrary!) ABIs
    * proper support for cross-compilation
        - ability to cross-compile exherbo to any desired target
        - ability to switch ABIs without re-installation
    * easier to add new ports/abis in the future
    * simple, uniform layout for all installations
    * BONUS! obsolete the multilib "hack" which is considered terrible even by
      upstream

# Required Changes
    * Path Changes:
        - PREFIX: /usr ↦ /usr/${CHOST}
        - ROOT: / ↦ /${CHOST} (see NOTE below on /usr merge)
    * Compiler Changes:
        - default value of PT_INTERP
        - default search path
    * libc changes:
        - **UNDO** changes done multilib on exherbo
    * Linker Changes
        - None!  This is a fully compatible change for the linker
    * New multibuild options
        - Introduction of new multibuild targets available on *all* platforms:
          + target (toolchain packages only)
          + host

# Deviation (with rationale) from Debian:
    Debian appends the CHOST to the paths rather than inserts the CHOST.  This
    results in a much larger change.  The proposed path changes for exherbo are
    more inline with supported/accepted/current glibc/gcc style.  Debian has
    decided to append to the path (e.g. /usr/bin becomes /usr/bin/${CHOST}).

    /usr/${CHOST} is used by gcc to support multiple cross-compilers residing on
    the same host.  By re-using the same paths we reduce changes, and maintain a
    much closer representation of the paths today.

    /usr/include simply becomes /usr/${CHOST}/include (which means that packages
    which use autotools dont even realise that we have changed the paths after
    the prefix change).

    This method also allows for the simplest change while requiring little
    change to most packages.  Packages which use autotools for building will
    implicitly be happy with this layout as different installation prefixes are
    already used (/usr/local is the *default* prefix, whilst installation to
    /usr is quite common).  It also results in a simpler change to gcc (most of
    which are due to hard-coded assumptions on prefix.)

    We retain the ability to use the same paths as today by means of symlink or
    bind mounting the current ABIs to the non-ABI paths (e.g. bind mount
    /usr/${CHOST}/bin to /usr/bin).

# Effects of /usr merge:
    In the event of a merge between root and /usr, the proposed multiarch layout
    becomes even simpler.  Everything resides under a /usr/${CHOST} prefix.
    /bin is simply /usr/${CHOST}/bin, /lib is simply /usr/${CHOST}/lib.

# Supporting "multilib" with multiarch:
    multilib is a special case co-installation of x86 libraries with x86_64.
    With multi-arch, due to the inserted CHOST, all installation becomes fully
    co-installable.  multilib simply becomes a degenerate case, involving the
    installation of x86 and x86_64.  This supports the current use case of
    running 32-bit binaries on a 64-bit installation.  Using parts, it is
    possible to avoid parallel installation of all components, and limit it to
    simply the headers and libraries per package.

# Behaviour of new multibuild options (hosts:*, targets:*)
    Each port becomes a hosts suboption.  Currently, exherbo supports the
    following:
        - i686-pc-linux-gnu
        - x86_64-pc-linux-gnu
        - arm-exherbo-linux-gnueabi

    While it may be possible to build the binaries for a given host, the system
    may not support execution of the binaries without emulation.  It is possible
    to use the binmisc format support of the Linux kernel to use an emulator to
    execute binaries for a different CPU target than the host.

    Each hosts suboption would require the equivalent targets suboption to be
    enabled in the toolchain packages.  Each toolchain is simply a
    cross-compiler toolchain designed to run on the system.

    Each package is (multi)built for the selected hosts.  With the ability to
    sysroot (chroot support in the package manager), it is possible to build an
    image for a different host value using just the toolchain on the host.

    Merging incompatible (e.g. arm-exherbo-linux-gnueabi to i686-pc-linux-gnu)
    targets to the host filesystem is possible, but unlikely to be desired for
    most users.  The functionality is useful for a certain class of developers
    who wish to have a parallel installation of an unexecutable "host" for
    testing and development purposes.  Execution in this scenario can be
    achieved via emulation (e.g. qemu-kvm + binfmt_misc).  This provides a
    simple mechanism for doing development for platforms via emulation or for
    platforms which are not yet available in testing or production hardware
    without a chroot.

    The "target" suboptions are for the toolchain packages (binutils, gcc)
    **ONLY**.  They control the hosts the toolchain package can build binaries
    for.

#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
